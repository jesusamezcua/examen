import conexion from '../userDB';

class NotasFunctions {

     public async fnListaNotas(iIdUsuario: number){
        const jsonResp = {
            intStatus: 500,
            msjResp: 'Error en la DB',
            jsonNotasResp: {}
        };
        try {
            var sql = "SELECT * FROM notas WHERE iIdUsuario = ?";
            let value = [iIdUsuario];
            let doQuery = (sql: any, value: any) => {
                return new Promise((resolve, reject) => {
                    conexion.query(sql,value, function (err: any, result: any, _fields: any) {
                        if(err) return reject(err);
                        return resolve(result);
                }) 
                });
            } 
            let result: any = await doQuery(sql,value);
            if(result.length == 0){
                jsonResp.intStatus = 201;
                jsonResp.msjResp = 'No tiene notas';
            }else{
                jsonResp.intStatus = 200;
                jsonResp.msjResp = 'Si tiene notas';
                jsonResp.jsonNotasResp = result;
            }
            return jsonResp;
        } catch (error) {
            console.log(error);
            jsonResp.msjResp = error;
            return jsonResp;
        }
    }

    public async fnEliminarNota(iIdNota: number){
        const jsonResp = {
            intStatus: 500,
            msjResp: 'Error en la DB',
            jsonNotasResp: {}
        };
        try {
            var sql = "DELETE FROM notas WHERE iIdNota = ?";
            let value = [iIdNota];
            let doQuery = (sql: any, value: any) => {
                return new Promise((resolve, reject) => {
                    conexion.query(sql,value, function (err: any, result: any, _fields: any) {
                        if(err) return reject(err);
                        return resolve(result);
                }) 
                });
            } 
            let result: any = await doQuery(sql,value);
            jsonResp.intStatus = 200;
            jsonResp.msjResp = 'Nota eliminada correctamente';
            return jsonResp;
        } catch (error) {
            console.log(error);
            jsonResp.msjResp = error;
            return jsonResp;
        }
    }

    public async fnAgregarNota(strTitulo: string, strContenido: string, iIdUsuario: number){
        const jsonResp = {
            intStatus: 500,
            msjResp: 'Error en la DB',
            jsonNotasResp: {}
        };
        try {
            var sql = "INSERT INTO notas (strTitulo, strContenido, iIdUsuario, created_at) VALUES (?,?,?,(?))";
            let value = [strTitulo, strContenido, iIdUsuario, new Date];
            let doQuery = (sql: any, value: any) => {
                return new Promise((resolve, reject) => {
                    conexion.query(sql,value, function (err: any, result: any, _fields: any) {
                        if(err) return reject(err);
                        return resolve(result);
                }) 
                });
            } 
            await doQuery(sql,value);
            jsonResp.intStatus = 200;
            jsonResp.msjResp = 'Nota agregada correctamente';
            return jsonResp;
        } catch (error) {
            console.log(error);
            jsonResp.msjResp = error;
            return jsonResp;
        }
    }

    public async fnObtenerNota(iIdNota: number){
        const jsonResp = {
            intStatus: 200,
            msjResp: 'Ok',
            jsonNotaRes: {}
        };
        try {
            var sql = "SELECT * FROM notas WHERE iIdNota = ? LIMIT 1";
            let value = [iIdNota];
            let doQuery = (sql: any, value: any) => {
                return new Promise((resolve, reject) => {
                    conexion.query(sql,value, function (err: any, result: any, _fields: any) {
                        if(err) return reject(err);
                        return resolve(result);
                }) 
                });
            } 
            let result: any = await doQuery(sql,value);
            if(result.length == 0){
                jsonResp.intStatus = 201;
                jsonResp.msjResp = 'No existe esa nota';
            }else{
                jsonResp.intStatus = 200;
                jsonResp.msjResp = 'Nota existente';
                jsonResp.jsonNotaRes = result[0];
            }
            return jsonResp;
        } catch (error) {
            console.log(error);
            jsonResp.msjResp = error;
            return jsonResp;
        }
    }

    public async fnModificarNota(strTitulo: string, strContenido: string, iIdNota: number){
        const jsonResp = {
            intStatus: 500,
            msjResp: 'Error en la DB',
            jsonNotasResp: {}
        };
        try {
            var sql = "UPDATE notas SET strTitulo = ?, strContenido = ? WHERE iIdNota = ?";
            let value = [strTitulo, strContenido, iIdNota];
            let doQuery = (sql: any, value: any) => {
                return new Promise((resolve, reject) => {
                    conexion.query(sql,value, function (err: any, result: any, _fields: any) {
                        if(err) return reject(err);
                        return resolve(result);
                }) 
                });
            } 
            await doQuery(sql,value);
            jsonResp.intStatus = 200;
            jsonResp.msjResp = 'Nota modificada correctamente';
            return jsonResp;
        } catch (error) {
            console.log(error);
            jsonResp.msjResp = error;
            return jsonResp;
        }
    }
}

const notasFunctions = new NotasFunctions();
export default notasFunctions;