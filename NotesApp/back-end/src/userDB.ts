var mysql = require('mysql');
var conexion= mysql.createConnection({
    host : 'localhost',
    database : 'notesappdb',
    user : 'root',
    password : 'user',
});

conexion.connect(function(err: { stack: string; }) {
    if (err) {
        console.error('Error de conexion: ' + err.stack);
        return;
    }
    console.log('Conectado con el identificador ' + conexion.threadId);
});

export default conexion;