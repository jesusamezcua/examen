import { Router } from 'express';
import notasController from '../controllers/notasController';

class NotasRoutes {

    public router: Router = Router();

    constructor() {
        this.config();
    }

    config(): void {
        this.router.get('/:iIdUsuario', notasController.fncListaNotas);
        this.router.delete('/eliminar/:iIdNota', notasController.fncEliminarNota);
        this.router.post('/agregar', notasController.fncAgregarNota);
        this.router.get('/obtener/:iIdNota', notasController.fncObtenerNota);
        this.router.put('/modificar', notasController.fncModificarNota);
    }

}

const notasRoutes = new NotasRoutes();
export default notasRoutes.router;