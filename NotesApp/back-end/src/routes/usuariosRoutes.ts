import { Router } from 'express';
import usuariosController from '../controllers/usuariosController';

class UsuariosRoutes {

    public router: Router = Router();

    constructor() {
        this.config();
    }

    config(): void {
        this.router.post('/Registro', usuariosController.fncRegisterUser);
        this.router.put('/Login', usuariosController.fncLogin);
    }

} 

const usuariosRoutes = new UsuariosRoutes();
export default usuariosRoutes.router;