import { Request, Response } from 'express';
import  usuariosFunctions  from '../functions/usuariosFunctions';

class UsuariosController {

    public async fncRegisterUser(req: Request, res: Response) {
        var strEmail = req.body.strEmail;
        var strPassword = req.body.strPassword;
        var jsonRes = await usuariosFunctions.fnRegisterUser(strEmail, strPassword);
        res.send(jsonRes); 
    }

    public async fncLogin(req: Request, res: Response) {
        var strEmail = req.body.strEmail;
        var strPassword = req.body.strPassword;
        var jsonEn = await usuariosFunctions.fnLogin(strEmail, strPassword);
        res.send(jsonEn);
    }
}

const usuariosController = new UsuariosController();
export default usuariosController;