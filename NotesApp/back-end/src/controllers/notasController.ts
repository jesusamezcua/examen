import {Request, Response } from 'express';
import notasFunctions from '../functions/notasFunctions';

class NotasController {
    public async fncListaNotas(req: Request, res: Response) {
        var iIdUsuario = req.params.iIdUsuario;
        var jsonRes = await notasFunctions.fnListaNotas(iIdUsuario);
        res.send(jsonRes);
    }

    public async fncEliminarNota(req: Request, res: Response) {
        var iIdNota = req.params.iIdNota;
        var jsonRes = await notasFunctions.fnEliminarNota(iIdNota);
        res.send(jsonRes);
    }

    public async fncAgregarNota(req: Request, res: Response) {
        var strTitulo = req.body.strTitulo;
        var strContenido = req.body.strContenido;
        var iIdUsuario = req.body.iIdUsuario;
        var jsonRes = await notasFunctions.fnAgregarNota(strTitulo, strContenido, iIdUsuario);
        res.send(jsonRes);
    }

    public async fncObtenerNota(req: Request, res: Response) {
        var iIdNota = req.params.iIdNota;
        var jsonRes = await notasFunctions.fnObtenerNota(iIdNota);
        res.send(jsonRes);
    }

    public async fncModificarNota(req: Request, res: Response) {
        var strTitulo = req.body.strTitulo;
        var strContenido = req.body.strContenido;
        var iIdNota = req.body.iIdNota;
        var jsonRes = await notasFunctions.fnModificarNota(strTitulo, strContenido, iIdNota);
        res.send(jsonRes);
    }

}

const notasController = new NotasController();
export default notasController;