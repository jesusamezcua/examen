"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const usuariosFunctions_1 = __importDefault(require("../functions/usuariosFunctions"));
class UsuariosController {
    fncRegisterUser(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            var strEmail = req.body.strEmail;
            var strPassword = req.body.strPassword;
            var jsonRes = yield usuariosFunctions_1.default.fnRegisterUser(strEmail, strPassword);
            res.send(jsonRes);
        });
    }
    fncLogin(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            var strEmail = req.body.strEmail;
            var strPassword = req.body.strPassword;
            var jsonEn = yield usuariosFunctions_1.default.fnLogin(strEmail, strPassword);
            res.send(jsonEn);
        });
    }
}
const usuariosController = new UsuariosController();
exports.default = usuariosController;
