"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
/*import pool from '../database';*/
const database_1 = __importDefault(require("../database"));
class EventosController {
    /*public async list (req: Request, res: Response) {
        const eventos = await pool.query('SELECT * FROM eventos');
        res.json(eventos);
    }

    public async getOne (req: Request, res: Response): Promise<any> {
        const { id } = req.params;
        const eventos = await pool.query('SELECT * FROM eventos WHERE id = ?', [id]);
        if (eventos.length > 0){
            return res.json(eventos[0]);
        }
        res.status(404).json({text: 'Evento no encontrado '});
    }

    public async create (req: Request, res: Response): Promise<void>{
        await pool.query('INSERT INTO eventos set ?', [req.body])
        res.json({message: 'Evento guardado'});
    }

    public async delete (req: Request, res: Response): Promise<void> {
        const { id } = req.params;
        await pool.query('DELETE FROM eventos WHERE id = ?', [id]);
        res.json({message: 'evento eliminado '});
    }
    public async update (req: Request, res: Response): Promise<void> {
        const { id } = req.params;
        await pool.query('UPDATE eventos SET ? WHERE id = ?', [req.body, id]);
        res.json({message: 'evento modificado'});
    }*/
    /////////////////////////*Mongo*/
    todosEventos(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            let eventos = yield database_1.default.find((err, eventos) => {
                if (err) {
                    res.send("Error!");
                }
                else {
                    res.send(eventos);
                }
            });
        });
    }
    ;
    nuevoEvento(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            var evento = new database_1.default(req.body);
            evento.save((err) => {
                if (err) {
                    res.send(err);
                }
                else {
                    res.send(evento);
                }
            });
        });
    }
    obtEvento(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            var evento = yield database_1.default.findOne({ _id: req.params._id }, (err, evento) => {
                if (err) {
                    res.send(err);
                }
                else {
                    res.send(evento);
                }
            });
        });
    }
    ;
    modEvento(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            yield database_1.default.update({ _id: req.params._id }, req.body, (err, evento) => {
                if (err) {
                    res.send(err);
                }
                else {
                    res.json({ message: 'evento modificado' });
                }
            });
        });
    }
    eliminarEvento(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            var evento = yield database_1.default.remove({ _id: req.params._id }, (err) => {
                if (err) {
                    res.send(err);
                }
                else {
                    res.json({ message: 'evento eliminado' });
                }
            });
        });
    }
}
const eventosController = new EventosController();
exports.default = eventosController;
