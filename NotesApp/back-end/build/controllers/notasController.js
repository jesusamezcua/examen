"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const notasFunctions_1 = __importDefault(require("../functions/notasFunctions"));
class NotasController {
    fncListaNotas(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            var iIdUsuario = req.params.iIdUsuario;
            var jsonRes = yield notasFunctions_1.default.fnListaNotas(iIdUsuario);
            res.send(jsonRes);
        });
    }
    fncEliminarNota(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            var iIdNota = req.params.iIdNota;
            var jsonRes = yield notasFunctions_1.default.fnEliminarNota(iIdNota);
            res.send(jsonRes);
        });
    }
    fncAgregarNota(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            var strTitulo = req.body.strTitulo;
            var strContenido = req.body.strContenido;
            var iIdUsuario = req.body.iIdUsuario;
            var jsonRes = yield notasFunctions_1.default.fnAgregarNota(strTitulo, strContenido, iIdUsuario);
            res.send(jsonRes);
        });
    }
    fncObtenerNota(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            var iIdNota = req.params.iIdNota;
            var jsonRes = yield notasFunctions_1.default.fnObtenerNota(iIdNota);
            res.send(jsonRes);
        });
    }
    fncModificarNota(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            var strTitulo = req.body.strTitulo;
            var strContenido = req.body.strContenido;
            var iIdNota = req.body.iIdNota;
            var jsonRes = yield notasFunctions_1.default.fnModificarNota(strTitulo, strContenido, iIdNota);
            res.send(jsonRes);
        });
    }
}
const notasController = new NotasController();
exports.default = notasController;
