"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var mysql = require('mysql');
var conexion = mysql.createConnection({
    host: 'localhost',
    database: 'notesappdb',
    user: 'root',
    password: 'user',
});
conexion.connect(function (err) {
    if (err) {
        console.error('Error de conexion: ' + err.stack);
        return;
    }
    console.log('Conectado con el identificador ' + conexion.threadId);
});
exports.default = conexion;
