"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const notasController_1 = __importDefault(require("../controllers/notasController"));
class NotasRoutes {
    constructor() {
        this.router = express_1.Router();
        this.config();
    }
    config() {
        this.router.get('/:iIdUsuario', notasController_1.default.fncListaNotas);
        this.router.delete('/eliminar/:iIdNota', notasController_1.default.fncEliminarNota);
        this.router.post('/agregar', notasController_1.default.fncAgregarNota);
        this.router.get('/obtener/:iIdNota', notasController_1.default.fncObtenerNota);
        this.router.put('/modificar', notasController_1.default.fncModificarNota);
    }
}
const notasRoutes = new NotasRoutes();
exports.default = notasRoutes.router;
