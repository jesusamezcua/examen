"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const eventosController_1 = __importDefault(require("../controllers/eventosController"));
class EventosRoutes {
    constructor() {
        this.router = express_1.Router();
        this.config();
    }
    config() {
        this.router.get('/', eventosController_1.default.todosEventos);
        this.router.post('/', eventosController_1.default.nuevoEvento);
        this.router.delete('/:_id', eventosController_1.default.eliminarEvento);
        this.router.put('/:_id', eventosController_1.default.modEvento);
        this.router.get('/:_id', eventosController_1.default.obtEvento);
    }
}
const eventosRoutes = new EventosRoutes();
exports.default = eventosRoutes.router;
