"use strict";
/*import mysql from 'promise-mysql';

import keys from './keys';

const pool = mysql.createPool(keys.database);

pool.getConnection()
    .then(connection => {
        pool.releaseConnection(connection);
        console.log('DB es conectada');
    });

    export default pool;*/
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.mong = void 0;
const mongoose_1 = __importDefault(require("mongoose"));
const URI = 'mongodb://localhost:27017/dbEventually';
mongoose_1.default.connect(URI, (err) => {
    if (err) {
        console.log(err.message);
    }
    else {
        console.log("conectado a mongo");
    }
});
exports.mong = new mongoose_1.default.Schema({
    title: { type: String, required: true },
    description: { type: String, required: true },
    image: { type: String, required: true },
    created_at: { type: String, required: true }
});
const eventoss = mongoose_1.default.model('eventos', exports.mong);
exports.default = eventoss;
