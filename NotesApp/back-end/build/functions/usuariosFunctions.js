"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const userDB_1 = __importDefault(require("../userDB"));
class UsuariosFunctions {
    fnRegisterUser(strEmail, strPassword) {
        return __awaiter(this, void 0, void 0, function* () {
            const jsonResp = {
                intStatus: 500,
                msjResp: 'Error en la DB'
            };
            let strToken = 'token12345' + strEmail + '54321token'; // no es correcto generar el token asi, solo es representativo
            try {
                var sql = "SELECT * FROM usuarios WHERE strEmail = ?";
                let value = [strEmail];
                let doQuery = (sql, value) => {
                    return new Promise((resolve, reject) => {
                        userDB_1.default.query(sql, value, function (err, result, _fields) {
                            if (err)
                                return reject(err);
                            return resolve(result);
                        });
                    });
                };
                let result = yield doQuery(sql, value);
                if (result.length > 0) {
                    jsonResp.intStatus = 201;
                    jsonResp.msjResp = 'El correo ya existe';
                }
                else {
                    sql = "INSERT INTO usuarios (strEmail, strPassword, strToken) VALUES (?,SHA1(?),?)";
                    let values = [strEmail, strPassword, strToken];
                    result = yield doQuery(sql, values);
                    jsonResp.intStatus = 200;
                    jsonResp.msjResp = 'Usuario registrado correctamente';
                }
                return jsonResp;
            }
            catch (error) {
                console.log(error);
                jsonResp.msjResp = error;
                return jsonResp;
            }
        });
    }
    fnLogin(strEmail, strPassword) {
        return __awaiter(this, void 0, void 0, function* () {
            const jsonResp = {
                intStatus: 200,
                msjResp: 'Ok',
                jsonUserRes: {}
            };
            try {
                var sql = "SELECT * FROM usuarios WHERE strEmail = ?";
                let value = [strEmail];
                let doQuery = (sql, value) => {
                    return new Promise((resolve, reject) => {
                        userDB_1.default.query(sql, value, function (err, result, _fields) {
                            if (err)
                                return reject(err);
                            return resolve(result);
                        });
                    });
                };
                let result = yield doQuery(sql, value);
                if (result.length > 0) {
                    sql = "SELECT * FROM usuarios WHERE strEmail = ? AND strPassword = SHA1(?) LIMIT 1";
                    let values = [strEmail, strPassword];
                    result = yield doQuery(sql, values);
                    if (result.length == 0) {
                        jsonResp.intStatus = 202;
                        jsonResp.msjResp = 'El correo o la contraseña son incorrectos';
                    }
                    else {
                        jsonResp.intStatus = 200;
                        jsonResp.msjResp = 'Usuario logueado correctamente';
                        jsonResp.jsonUserRes = result[0];
                        console.log(result[0]);
                    }
                }
                else {
                    jsonResp.intStatus = 201;
                    jsonResp.msjResp = 'El correo no existe';
                }
                return jsonResp;
            }
            catch (error) {
                console.log(error);
                jsonResp.msjResp = error;
                return jsonResp;
            }
        });
    }
    ;
}
const usuariosFunctions = new UsuariosFunctions();
exports.default = usuariosFunctions;
