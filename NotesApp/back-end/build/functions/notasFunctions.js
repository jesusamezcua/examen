"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const userDB_1 = __importDefault(require("../userDB"));
class NotasFunctions {
    fnListaNotas(iIdUsuario) {
        return __awaiter(this, void 0, void 0, function* () {
            const jsonResp = {
                intStatus: 500,
                msjResp: 'Error en la DB',
                jsonNotasResp: {}
            };
            try {
                var sql = "SELECT * FROM notas WHERE iIdUsuario = ?";
                let value = [iIdUsuario];
                let doQuery = (sql, value) => {
                    return new Promise((resolve, reject) => {
                        userDB_1.default.query(sql, value, function (err, result, _fields) {
                            if (err)
                                return reject(err);
                            return resolve(result);
                        });
                    });
                };
                let result = yield doQuery(sql, value);
                if (result.length == 0) {
                    jsonResp.intStatus = 201;
                    jsonResp.msjResp = 'No tiene notas';
                }
                else {
                    jsonResp.intStatus = 200;
                    jsonResp.msjResp = 'Si tiene notas';
                    jsonResp.jsonNotasResp = result;
                }
                return jsonResp;
            }
            catch (error) {
                console.log(error);
                jsonResp.msjResp = error;
                return jsonResp;
            }
        });
    }
    fnEliminarNota(iIdNota) {
        return __awaiter(this, void 0, void 0, function* () {
            const jsonResp = {
                intStatus: 500,
                msjResp: 'Error en la DB',
                jsonNotasResp: {}
            };
            try {
                var sql = "DELETE FROM notas WHERE iIdNota = ?";
                let value = [iIdNota];
                let doQuery = (sql, value) => {
                    return new Promise((resolve, reject) => {
                        userDB_1.default.query(sql, value, function (err, result, _fields) {
                            if (err)
                                return reject(err);
                            return resolve(result);
                        });
                    });
                };
                let result = yield doQuery(sql, value);
                jsonResp.intStatus = 200;
                jsonResp.msjResp = 'Nota eliminada correctamente';
                return jsonResp;
            }
            catch (error) {
                console.log(error);
                jsonResp.msjResp = error;
                return jsonResp;
            }
        });
    }
    fnAgregarNota(strTitulo, strContenido, iIdUsuario) {
        return __awaiter(this, void 0, void 0, function* () {
            const jsonResp = {
                intStatus: 500,
                msjResp: 'Error en la DB',
                jsonNotasResp: {}
            };
            try {
                var sql = "INSERT INTO notas (strTitulo, strContenido, iIdUsuario, created_at) VALUES (?,?,?,(?))";
                let value = [strTitulo, strContenido, iIdUsuario, new Date];
                let doQuery = (sql, value) => {
                    return new Promise((resolve, reject) => {
                        userDB_1.default.query(sql, value, function (err, result, _fields) {
                            if (err)
                                return reject(err);
                            return resolve(result);
                        });
                    });
                };
                yield doQuery(sql, value);
                jsonResp.intStatus = 200;
                jsonResp.msjResp = 'Nota agregada correctamente';
                return jsonResp;
            }
            catch (error) {
                console.log(error);
                jsonResp.msjResp = error;
                return jsonResp;
            }
        });
    }
    fnObtenerNota(iIdNota) {
        return __awaiter(this, void 0, void 0, function* () {
            const jsonResp = {
                intStatus: 200,
                msjResp: 'Ok',
                jsonNotaRes: {}
            };
            try {
                var sql = "SELECT * FROM notas WHERE iIdNota = ? LIMIT 1";
                let value = [iIdNota];
                let doQuery = (sql, value) => {
                    return new Promise((resolve, reject) => {
                        userDB_1.default.query(sql, value, function (err, result, _fields) {
                            if (err)
                                return reject(err);
                            return resolve(result);
                        });
                    });
                };
                let result = yield doQuery(sql, value);
                if (result.length == 0) {
                    jsonResp.intStatus = 201;
                    jsonResp.msjResp = 'No existe esa nota';
                }
                else {
                    jsonResp.intStatus = 200;
                    jsonResp.msjResp = 'Nota existente';
                    jsonResp.jsonNotaRes = result[0];
                }
                return jsonResp;
            }
            catch (error) {
                console.log(error);
                jsonResp.msjResp = error;
                return jsonResp;
            }
        });
    }
    fnModificarNota(strTitulo, strContenido, iIdNota) {
        return __awaiter(this, void 0, void 0, function* () {
            const jsonResp = {
                intStatus: 500,
                msjResp: 'Error en la DB',
                jsonNotasResp: {}
            };
            try {
                var sql = "UPDATE notas SET strTitulo = ?, strContenido = ? WHERE iIdNota = ?";
                let value = [strTitulo, strContenido, iIdNota];
                let doQuery = (sql, value) => {
                    return new Promise((resolve, reject) => {
                        userDB_1.default.query(sql, value, function (err, result, _fields) {
                            if (err)
                                return reject(err);
                            return resolve(result);
                        });
                    });
                };
                yield doQuery(sql, value);
                jsonResp.intStatus = 200;
                jsonResp.msjResp = 'Nota modificada correctamente';
                return jsonResp;
            }
            catch (error) {
                console.log(error);
                jsonResp.msjResp = error;
                return jsonResp;
            }
        });
    }
}
const notasFunctions = new NotasFunctions();
exports.default = notasFunctions;
