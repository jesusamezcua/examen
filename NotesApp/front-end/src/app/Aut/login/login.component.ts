import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Usuario } from 'src/app/Modelos/Usuario/usuario';
import { UsuarioService } from 'src/app/Servicios/usuario.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  formLogin = new FormGroup({
    strEmail: new FormControl('', [Validators.required, Validators.email]),
    strPassword: new FormControl('', [Validators.required])
  });
  
  constructor(public userService: UsuarioService, public router: Router) { }

  ngOnInit() {
  }

  async fnLogin() {
    try {
      let user:Usuario = new Usuario();
      user.strEmail = this.formLogin.get('strEmail').value;
      user.strPassword = this.formLogin.get('strPassword').value;
      let res = await this.userService.fnLogin(user);
      if(res['intStatus'] == 200){
        console.log(res['jsonUserRes']);
        this.userService.setToken(res['jsonUserRes']['strToken'], res['jsonUserRes']['iIdUsuario']);
        this.router.navigateByUrl('/');
      }else{
        alert("Oh Oh!!! " + res['msjResp']);
      }
    } catch (error) {
      alert("Error en el servidor !!!");
    }
  }
}
