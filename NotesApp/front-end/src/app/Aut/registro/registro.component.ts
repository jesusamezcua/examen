import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Usuario } from 'src/app/Modelos/Usuario/usuario';
import { UsuarioService } from 'src/app/Servicios/usuario.service';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css']
})
export class RegistroComponent implements OnInit {

  passwordCorrect:boolean =true;
  formRegistrarUsuario = new FormGroup({
    strEmail: new FormControl('', [Validators.required, Validators.email]),
    strPassword: new FormControl('', [Validators.required, Validators.minLength(5)]),
    strPasswordConfirm: new FormControl('', [Validators.required]),
  });

  constructor(public userService: UsuarioService, public router: Router) { }

  ngOnInit() {
  }

  async fnRegistro() {
    try {
      let user:Usuario = new Usuario();
      user.strEmail = this.formRegistrarUsuario.get('strEmail').value;
      user.strPassword = this.formRegistrarUsuario.get('strPassword').value;
      let res = await this.userService.fnRegistro(user);
      console.log(res)
      if(res['intStatus'] == 200){
        alert("Usuario registrado correctamente");
        this.router.navigateByUrl('/login');
      }else{
        alert("Oh Oh!!! " + res['msjResp']);
      }
    } catch (error) {
      console.log(error)
      alert("Error en el servidor !!!");
    }
  }

  //Validar que la contraseña contenga numero y letras
  fnValidarNumeroPassword(e) {
    var text = this.formRegistrarUsuario.get('strPassword').value;
    var numberUser = parseInt(text.toString().replace(/[^0-9]/g, ''))
    if (numberUser && text.length >= 6) {
      this.passwordCorrect = true;
    } else {
      this.passwordCorrect = false;
    }
  }

  //Validar que las contraseñas sea iguales
  fnValidarPassword(e) {
    var password = this.formRegistrarUsuario.get('strPassword').value;
    var confirmPassword = this.formRegistrarUsuario.get('strPasswordConfirm').value;
    if (password !== confirmPassword) {
      this.formRegistrarUsuario.get('strPasswordConfirm').setErrors({ NoPassswordMatch: true });
    }
  }
}