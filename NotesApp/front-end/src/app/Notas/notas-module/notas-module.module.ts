import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { AddNotasComponent } from '../add-notas/add-notas.component';
import { HttpClientModule } from '@angular/common/http';
import { ListNotasComponent } from '../list-notas/list-notas.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

const routes: Routes = [
  { path: 'agregarNota', component: AddNotasComponent },
  { path: 'agregarNota/:iIdNota', component: AddNotasComponent },
  { path: '', component: ListNotasComponent }
] 

@NgModule({
  declarations: [AddNotasComponent,
    ListNotasComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
    HttpClientModule
  ],
  exports: [],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
  ]
})
export class NotasModuleModule { }