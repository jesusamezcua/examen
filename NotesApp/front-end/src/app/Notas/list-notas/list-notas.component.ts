import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Notas } from 'src/app/Modelos/Notas/notas';
import { NotasService } from 'src/app/Servicios/notas.service';
import { UsuarioService } from 'src/app/Servicios/usuario.service';

@Component({
  selector: 'app-list-notas',
  templateUrl: './list-notas.component.html',
  styleUrls: ['./list-notas.component.css']
})
export class ListNotasComponent implements OnInit {

  constructor(public dataservice: NotasService, public userService: UsuarioService, public router: Router) { }
  
  listaNotas: Notas[] = [];
  boolDatos: Boolean = false;
  
  ngOnInit() {
    this.fnCargarNotas();
  }

  async fnCargarNotas() {
    try {
      let res = await this.dataservice.fnObtenerNotas();
      if(res['intStatus'] == 200 || res['intStatus'] == 201){ //200 si tiene notas guardadas y 201 si no tiene notas guardadas
        this.listaNotas = res['jsonNotasResp'];
        if (this.listaNotas.length > 0) {
          this.boolDatos = true;
        }
        else {
          this.boolDatos = false;
        }
      }else{
        alert("Oh Oh!!! " + res['msjResp']);
      }
    } catch (error) {
      alert("Error en el servidor !!!");
    }
  }

  async fnCerrarSesion(){
    await this.userService.fnLogout();
    this.router.navigateByUrl('/login');
  }

  async fnEliminarNota(iIdNota: number) {
    try {
      if (confirm("Estas seguro que deseas eliminar la nota?")) {
        let res = await this.dataservice.fnEliminarNota(iIdNota);
        if(res['intStatus'] == 200){
          this.fnCargarNotas();
        }else{
          alert("Oh Oh!!! " + res['msjResp']);
        }
      }
    } catch (error) {
      alert("Error en el servidor !!!");
    }
  }
}