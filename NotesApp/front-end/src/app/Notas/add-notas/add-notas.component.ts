import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Notas } from 'src/app/Modelos/Notas/notas';
import { NotasService } from 'src/app/Servicios/notas.service';

@Component({
  selector: 'app-add-notas',
  templateUrl: './add-notas.component.html',
  styleUrls: ['./add-notas.component.css']
})
export class AddNotasComponent implements OnInit {

  private suscribe: Subscription;
  constructor(private routerService: Router, private routeService: ActivatedRoute, public dataservice: NotasService) { 
    this.suscribe = new Subscription();
  }

  boolEditar: boolean = false;
  iIdNota: number;

  formAgregarNota = new FormGroup({
    strTitulo: new FormControl('', [Validators.required]),
    strContenido: new FormControl('', [ Validators.required])
  });

  async ngOnInit() {
    await this.suscribe.add(
      this.routeService.paramMap.subscribe(params => {
        if (params.get('iIdNota')) {
          this.iIdNota =  parseInt(params.get('iIdNota'));
          this.fnCargarNota();
          this.boolEditar = true;
        }
      }))
  }

  async fnCargarNota(){
    try {
      let res = await this.dataservice.fnObtenerNota(this.iIdNota);
      if(res['intStatus'] == 200){
        let nota: Notas = res['jsonNotaRes'];
        this.formAgregarNota.controls['strContenido'].setValue(nota['strContenido']);
        this.formAgregarNota.controls['strTitulo'].setValue(nota['strTitulo']);
      }else{
        alert("Oh Oh!!! " + res['msjResp']);
      }
    } catch (error) {
      alert("Error en el servidor !!!");
    }
  }

  async fnGuardar(){
    try {
      let nota: Notas = new Notas(this.formAgregarNota.value);
      nota.iIdUsuario = parseInt(localStorage.getItem("iIdUsuario"));
      let res = await this.dataservice.fnAgregarNota(nota);
      if(res['intStatus'] == 200){
        this.routerService.navigateByUrl("");
      }else{
        alert("Oh Oh!!! " + res['msjResp']);
      }
    } catch (error) {
      alert("Error en el servidor !!!");
    }
  }

  async fnEditar(){
    try {
      let nota: Notas = new Notas(this.formAgregarNota.value);
      nota.iIdNota = this.iIdNota;
      let res = await this.dataservice.fnModificarNota(nota);
      console.log(res)
      if(res['intStatus'] == 200){
        this.routerService.navigateByUrl("");
      }else{
        alert("Oh Oh!!! " + res['msjResp']);
      }
    } catch (error) {
      alert("Error en el servidor !!!");
    }
  }
}