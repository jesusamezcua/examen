import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Notas } from '../Modelos/Notas/notas';

@Injectable({
  providedIn: 'root'
})
export class NotasService {

  readonly strUrl = environment.strUrl;
  constructor(private http: HttpClient) {
  }

  public fnObtenerNotas() {
    let iIdUsuario = localStorage.getItem("iIdUsuario");
    return this.http.get(this.strUrl+'/Notas/'+iIdUsuario).toPromise();
  }

  public fnEliminarNota(iIdNota: number) {
    return this.http.delete(this.strUrl+'/Notas/eliminar/'+iIdNota).toPromise();
  }

  public fnAgregarNota(Nota: Notas){
    return this.http.post(this.strUrl+'/Notas/agregar', Nota).toPromise();
  }

  public fnObtenerNota(iIdNota: number){
    return this.http.get(this.strUrl+'/Notas/obtener/'+iIdNota).toPromise();
  }

  public fnModificarNota(Nota: Notas) {
    return this.http.put(this.strUrl+'/Notas/modificar', Nota).toPromise();
  }
}
