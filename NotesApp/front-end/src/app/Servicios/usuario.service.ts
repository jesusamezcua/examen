import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Usuario } from '../Modelos/Usuario/usuario';
import { CookieService } from "ngx-cookie-service";

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  readonly strUrl = environment.strUrl;
  
  constructor(private http: HttpClient, private cookies: CookieService) { }

  fnLogin(user: Usuario) {
    return this.http.put(this.strUrl+'/Auth/Login', user).toPromise();
  }

  fnRegistro(user: Usuario) {
    return this.http.post(this.strUrl+'/Auth/Registro', user).toPromise();
  }

  setToken(token, iIdusuario) {
    localStorage.setItem("token", token);
    localStorage.setItem("iIdUsuario", iIdusuario);
  }
  getToken() {
    return localStorage.getItem("token");
  }

  fnLogout() {
    delete localStorage.token;
    delete localStorage.iIdUsuario;
  }
}
