export class Notas {
    iIdNota:number = 0;
    strTitulo:string = '';
    strContenido:string = '';
    iIdUsuario:number = 0;
    created_at:Date= new Date();

    constructor(jsnForm: object) {
        this.iIdNota = 0;
        this.strTitulo = jsnForm['strTitulo'];
        this.strContenido = jsnForm['strContenido'];
        this.iIdUsuario = 0;
        this.created_at = new Date();
    }
}
