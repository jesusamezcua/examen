CREATE SCHEMA `notesappdb` DEFAULT CHARACTER SET utf8 ;

ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY 'user';

CREATE TABLE usuarios (
    iIdUsuario INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    strEmail VARCHAR(100),
    strPassword CHAR(56),
    strToken VARCHAR(150)
);

CREATE TABLE notas (
    iIdNota INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    strTitulo VARCHAR(100),
    strContenido VARCHAR(250),
    iIdUsuario INT UNSIGNED,
    created_at TIMESTAMP NULL,
    FOREIGN KEY (iIdUsuario) REFERENCES usuarios (iIdUsuario)
);

